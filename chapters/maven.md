# Maven

* [Офіційна сторінка.](https://maven.apache.org/)

## Часто вживані команди

* `mvn clean` очистити. Видаляє папку target/.
* `mvn package` компіляція і збір проекта. Найчастіше, отримуємо архів jar, war, ear.