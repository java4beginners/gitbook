# JDBC

Java DataBase Connectivity - стандарт Java для роботи з базами даних.
Надає мінімалістичний інтерфейс для стандартних операцій вибірки, вставки, видалення і тд.

Спробуємо ознайомитися з можливостями JDBC на прикладі.

#### Вправа 1

1. Зкачати код:  
`git clone https://gitlab.com/java4beginners/jdbc.git`

2. Перейти в директорію проекту:  
`cd jdbc-start/`

3. Запустити проект:  
`sh start.sh` 
  або `mvn clean package && mvn exec:java -Dexec.mainClass="ua.com.java4beginners.App"` 

4. Модифікувати код так, щоб в консолі ми бачили кількість записів в таблиці products. Написати необхідний select.


#### JDBC + Reflection

Розглянемо наступний метод

```java
public <E> List<E> select(String sql, Class<E> entityClass) {
        List<E> result = Lists.newArrayList();

        try {
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                result.add(parseResultSet(rs, entityClass));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T parseResultSet(ResultSet rs, Class<T> entityClass) {
        try {
            T result = entityClass.newInstance();
            Field[] fields = entityClass.getDeclaredFields();

            for (Field field : fields) {
                String columnName = getColumnName(field);
                Class<?> type = field.getType();

                field.setAccessible(true);
                if (type.equals(Long.class)) {
                    Long value = asLong(rs, columnName);
                    if (value == null) continue;
                    field.set(result, value);
                } else if (type.equals(String.class)) {
                    String value = asString(rs, columnName);
                    if (value == null) continue;
                    field.set(result, value);
                } else if (type.isEnum()) {
                    String value = asString(rs, columnName);
                    if (value == null) continue;
                    Enum enumVal = Enum.valueOf((Class<Enum>) type, value);
                    field.set(result, enumVal);
                }
            }
            return result;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
```

Даний метод приймає SQL запит як рядок і конвертує результат в список об'єктів класу E.

Спробуйте запустити і продебажити цей приклад.

#### Вправа 2

Спробуйте написати аналогічний код який робить вставку.
Сигнатура методу наступна:

```java
  public <E> void insert(E entity) { 
    ....
  }
```

Назва класу співпадає з назвою таблиці, ігноруючи регістр. Аналогічно для полів.