# SQL

Для роботи з базами даних необхідно встановити наприклад [MySQL](https://dev.mysql.com/downloads/installer/) server,
а також [Workbanch](https://dev.mysql.com/downloads/workbench/) - для запуску SQL запитів.


* [Tutorial point](https://www.tutorialspoint.com/sql/)
* [SQL course](http://www.sqlcourse.com/table.html)
* [geeksforgeeks](https://www.geeksforgeeks.org/sql-tutorial/)
* [Типи даних в MySQL](https://dev.mysql.com/doc/refman/8.0/en/data-types.html)
* [MySQL by Examples for Beginners](http://www.ntu.edu.sg/home/ehchua/programming/sql/mysql_beginner.html)


#### Встановлення.

Є декілька варіантів встановлення сервара MySQL:

1. Через інсталятор з офіційного сайта [MySQL](https://dev.mysql.com/downloads/installer/).
2. Використовуючи [Docker образ](https://hub.docker.com/r/mysql/mysql-server).
  Docker це інструмент віртуалізації програмного середовища.
  Спочатку встановлюєте [Docker](https://docs.docker.com/docker-for-windows/install/).
  З командного рядка набираєте `docker pull mysql/mysql-server`. 
  Зкачується образ. Запускаєте його `docker run --name=mysql1 -d mysql/mysql-server`.
  Логінетеся до сервера `docker exec -it mysql1 mysql -uroot -p`.
3. Можна спробувати хмарні варіанти, наприклад `https://www.db4free.net/`. Тільки для навчання.


#### Створення Бази Даних.

Схема/база це набір таблиць, користувачів, процедур, тригерів, прав і багато інших об'єктів об'єднаних одним ім'ям.
Після логіну виконаємо наступну команду:

```sql
CREATE DATABASE STORE; -- викорується один раз.
USE STORE; -- виконується при кожному логіні.
```

#### Створення таблиці. Вставка. Вибірка.

Будемо відпрацьовувати навички роботи з SQL на прикладі інтернет магазину.
Створемо таблицю для зберечення продуктів.

```sql
CREATE TABLE IF NOT EXISTS PRODUCTS (
    ID BIGINT AUTO_INCREMENT PRIMARY KEY, -- унікальний ІД-номер запису з автоматичним інкрементом при вставці
    TITLE VARCHAR(255) NOT NULL, -- назва продукту. Рядок до 255 символів. вставити NULL неможливо.
    DESCRIPTION TEXT, -- опис. Рядок довільної довжини.
    PRICE BIGINT NOT NULL, -- тип даних BIGINT це аналог Long в Java. 
    -- Ціна це дробове значення тому будемо зберігати в копійках. 
    -- Перед показом ділити на 100.
    IMG_URL VARCHAR(2048) NOT NULL -- посилання на картинку.
)  ENGINE=INNODB; -- внутрішній формат таблиці.
```

Зробимо декілька вставок в таблицю продуктів.

```sql
INSERT INTO PRODUCTS(TITLE, DESCRIPTION, PRICE, IMG_URL) VALUES ('Note Book Moleskin', 'High qyality book for notes.', 50000, '/img/office/moleskin.jpg');
```

Перевіримо чи з'явилися дані в таблиці. 

```sql
SELECT * FROM PRODUCTS;
```

Отримуємо

```
+----+--------------------+------------------------------+-------+--------------------------+
| ID | TITLE              | DESCRIPTION                  | PRICE | IMG_URL                  |
+----+--------------------+------------------------------+-------+--------------------------+
|  1 | Note Book Moleskin | High qyality book for notes. | 50000 | /img/office/moleskin.jpg |
+----+--------------------+------------------------------+-------+--------------------------+
1 row in set (0.00 sec)

```

Інший варіант з переліком полів.

```
mysql> SELECT ID, TITLE, PRICE/100 FROM PRODUCTS;
+----+--------------------+-----------+
| ID | TITLE              | PRICE/100 |
+----+--------------------+-----------+
|  1 | Note Book Moleskin |  500.0000 |
+----+--------------------+-----------+
1 row in set (0.00 sec)
```

Зробимо ще одну вставку.
```sql
INSERT INTO PRODUCTS(TITLE, DESCRIPTION, PRICE, IMG_URL) VALUES ('Note Book Moleskin A4', 'High qyality book for notes. A4 format', 60000, '/img/office/moleskin-a4.jpg');
```

Перевірте стан бази тепер.

Зробимо оновлення ціни для першого блокнота.

```sql
UPDATE PRODUCTS SET PRICE=55000 WHERE ID=1;
```

Зробіть вибірку зараз.

Видалимо перший запис.

```sql
DELETE FROM PRODUCTS WHERE ID=1;
```

Це самі базові операції необхідні для роботи з таблицями.


#### Відношення: ONE-TO-MANY

Створимо таблицю CUSTOMERS де будемо зберігати інформацію про клієнтів інтернет магазину.

```sql
CREATE TABLE IF NOT EXISTS CUSTOMERS (
    ID BIGINT AUTO_INCREMENT PRIMARY KEY,
    FIRST_NAME VARCHAR(255) NOT NULL,
    MIDDLE_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255) NOT NULL,
    PHONE CHAR(16) NOT NULL,
    EMAIL CHAR(128) NOT NULL UNIQUE,
    PASSWD VARCHAR(2048) NOT NULL
) ENGINE=INNODB;
```

Кожний користувач може створити скільки завгодно замовлень. 
Замовлення зберігаємо в `ORDERS`. 
Відповідно для одного запису в CUSTOMERS може бути багато записів в ORDERS.
Таку організацію даних і відношення між даними таблиці називають `ONE-TO-MANY`.

```sql
CREATE TABLE IF NOT EXISTS ORDERS (
    ID BIGINT AUTO_INCREMENT PRIMARY KEY,
    ORDER_TIME BIGINT NOT NULL DEFAULT (UNIX_TIMESTAMP()*1000),
    CUSTOMER_ID BIGINT NOT NULL,
    SHIPPING_ADDRESS VARCHAR(4096),
    ORDER_STATUS CHAR(64) NOT NULL,
    FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMERS(ID)
) ENGINE=INNODB;
```

Додамо дані.

```sql
INSERT INTO CUSTOMERS(FIRST_NAME, LAST_NAME, PHONE, EMAIL, PASSWD) VALUES('Ivan', 'Sydorenko', '0981234567', 'is@gmail.com', 'is');
INSERT INTO CUSTOMERS(FIRST_NAME, LAST_NAME, PHONE, EMAIL, PASSWD) VALUES('Mykola', 'Deineko', '066002233555', 'mdeineko@yahoo.com', 'mdeineko');
INSERT INTO ORDERS(CUSTOMER_ID, ORDER_STATUS) VALUES(1, 'PENDING');
INSERT INTO ORDERS(CUSTOMER_ID, ORDER_STATUS) VALUES(1, 'ON_THE_WAY');
INSERT INTO ORDERS(CUSTOMER_ID, ORDER_STATUS) VALUES(2, 'PENDING');
```

Бачимо, що у Івана 2 замовлення, а у Миколи одне.

#### INNER JOIN

Ми хочемо зробити запит, і отримати всі замовлення в статусі PENDING разом з ім'ям і прізвищем.
Для цього нам неоюхідно з'єднати 2 таблиці по певній умові.
Такі конструкції(з'єднання) називаються JOIN.

```sql
SELECT C.FIRST_NAME, C.LAST_NAME, O.ORDER_STATUS FROM CUSTOMERS C
    INNER JOIN ORDERS O ON C.ID = O.CUSTOMER_ID
    WHERE O.ORDER_STATUS = 'PENDING';
```

Результат.

```
+------------+-----------+--------------+
| FIRST_NAME | LAST_NAME | ORDER_STATUS |
+------------+-----------+--------------+
| Ivan       | Sydorenko | PENDING      |
| Mykola     | Deineko   | PENDING      |
+------------+-----------+--------------+
```

#### Відношення: MANY-TO-MANY

Один продукт може знаходитися в багатьох замовленнях і в одному замовлення може знаходитися багато продуктів.
Таке відношення між сутностями називають багато-до-багатьох (many-to-many).
Для реалізації нам знадобиться додаткова таблиця, що поєднує PRODUCTS i ORDERS.

```sql
CREATE TABLE IF NOT EXISTS PRODUCT_ORDER_MAP (
    PRODUCT_ID BIGINT NOT NULL,
    ORDER_ID BIGINT NOT NULL,
    FOREIGN KEY(PRODUCT_ID) REFERENCES PRODUCTS(ID) ON DELETE CASCADE,
    FOREIGN KEY(ORDER_ID) REFERENCES ORDERS(ID) ON DELETE CASCADE
) ENGINE=INNODB;
```

Додамо інформацію.

```SQL
INSERT INTO PRODUCT_ORDER_MAP VALUES(2, 1);
INSERT INTO PRODUCT_ORDER_MAP VALUES(2, 1);
INSERT INTO PRODUCT_ORDER_MAP VALUES(2, 3);
```

По кожному замовленню виведемо прізвище, статус і кількість продуктів.

```sql
SELECT O.ID AS `ORDER`, C.LAST_NAME AS NAME, O.ORDER_STATUS AS STATUS, COUNT(PO.PRODUCT_ID) AS COUNT
    FROM ORDERS O
    LEFT JOIN PRODUCT_ORDER_MAP PO ON O.ID = PO.ORDER_ID -- спробуйте запустити з INNER
    INNER JOIN CUSTOMERS C ON C.ID = O.CUSTOMER_ID
    GROUP BY `ORDER`, NAME, STATUS;
```

Результат

```
+-------+-----------+------------+-------+
| ORDER | NAME      | STATUS     | COUNT |
+-------+-----------+------------+-------+
|     1 | Sydorenko | PENDING    |     2 |
|     2 | Sydorenko | ON_THE_WAY |     0 |
|     3 | Deineko   | PENDING    |     1 |
+-------+-----------+------------+-------+
```