# Spring

Перше з чим варто розібратися - шаблон Dependency Injection.
Суть дуже проста. В класі Main ми використовуємо тип A, але в майбутньому прийдеться замінити на В.
Якщо ми пропишемо `new A()`, то пізніше маємо замінити по всьому проекту на `new B()`.
Проекти великі, мають багато залежностей. Це незручно.

Тому, що роблять. Тип А і В реалізуть інтерфейс `I`. Ним і оперують в класі Main.
А хтось ззовні створює `I instance = new A()` або `I instance = new B()` і передає нам.
Тобто клас Main навіть незнає який тим використовує А чи В. Ми винесли оператор `new` за межі класу.
І переклали відповідальність за створення об'єктів на фремворк (Spring).

#### Вправа 1
 1. Клонувати репозіторій `https://gitlab.com/java4beginners/spring.git`.
 2. Перейти в проект `spring-ioc/`.
 3. Зібрати проект `mvn clean package`.
 4. Запустити клас `App`.
 5. Почитати про анотацію @Provided i @Named. Використати. Переконатися, що працює. 