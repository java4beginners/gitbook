# Об'єкти і Зв'язані списки.

Основна задача цього розділу - сформувати бачення об'єктів і посилань в памяті комп'ютера.
Задачі з теми - зв'язані списки.

Дано клас `Node`:

```java
public class Node {
  int val;
  Node next;

  public Node(int val) {
    this.val = val;
  }
}
```

Створюємо список:

```java
Node head = new Node(1);
Node second = new Node(2);
head.next = second;
Node third = new Node(3);
second.next = third;
third.next = new Node(44);
third.next.next = new Node(555);
third.next.next.next = new Node(6);
```

Графічно, таку структуру, можна зобразити наступним чином:

```
(1)-->(2)-->(3)-->(44)-->(555)-->(6)-->null
```


### Задачі

1. Дано зв'язаний список. Видалити перший елемент.
2. Дано зв'язаній список. Видалини другий елемент.
3. Вставити новий елемент на третю позицію.
4. Вставити новий елемент на `i`-ту позицію.
3. Із списку видалити `i`-й елемент.
4. Дано список. Змінити порядок елементів в списку. Неможна використовувати масив та перестворювати елементи, змінювати значення.
    Тільки модифікувати посилання.
5. Дано два зв'язаних списка відсортованих по зростанню. Необхидно виконати операцію злиття(merge). Має утворитися один, відсортований список.
6. Визначити. Чи є цикл в списку.
7. Змінити порядок вузлів, що стоять на непарних позиціях.
8. Видалити дублікати у відсортованому списку.
