# Питання на співбесіді

### Core

##### Примітивні типи

1. Які примітивні типи даних ви знаєте? Перерахуйте. Скільки байт займає кожен.
2. Що таке літерал.
3. Скільки буде `(int)('z' - 'a')`?
4. Чим відрізняється `++a` i `a++`?
5. Який результат роботи такого коду: 
   ```java
   byte b = 1;
   while(b < 128) {
     b++;
   }
   ```
6. Що роблять операції `>>` `<<` `>>>` `!` `~` `^`.
7. Скільки буде `Math.abs(Integer.MIN_VALUE)`?
8. Скільки приблизно буде `(Integer.MAX_VALUE + 1)/2`?
9. Чому дорівнює `3/2` і `3/2.0`. Поясніть як це працює.
10. Скільки приблизно буде `(Integer.MAX_VALUE + 1) >>> 1`?

##### Об'єкти

1. Методи класа Object.
2. Контракт між hashCode і equals
3. Як працює HashMap.
4. Що буде якщо всі об'єкти повертають константний hashCode.
5. За який час працює HashMap, ArrayList, LinkedList, Heap/PriorityQueue.
6. Відношення між класами `has a` vs `is a`. Агрегація vs композиція.
7. Що таке дефолтний конструктор? Коли його не буде?
8. Виклик конструкторів батьківських класів.
9. Виклик одного конструктора із іншого, того ж класу.
6. Які є модифікатори доступа, чим відрізняються.
7. Що таке анонімний клас.
8. Статичні поля і методи.
9. Чим відрізняється статичний і нестатичний внутрішній клас.
10. final. Термінальні класи і методи.
12. Override vs Overload
13. Анонімні класи.
14. Як JVM ділить память. Які є розділи і для чого?
15. Що теке КласЛоадери. І для чого?

##### Потоки

1. volotile
2. final and object publishing.
3. "happens before". Поясніть
4. Різниця потока від процеса. Конкретно!
5. thread.run() vs thread.start()
6. Що буде якщо викликати start() декілька разів?
7. Стани потока.
8. Dead lock. Приклад коду.
9. Атомік-класи. Як це працює.
10. synchronized блок vs метод vs статичний метод.
11. Один потік збільшує змінну на 1, інший зменшує ту ж саму змінну на 1. Після відпрацювання потоків, які варіанти можуть бути.
12. ...

### ООП

1. Інкапсуляція, наслідування, поліморфізм.
2. Абстракція
3. SOLID
4. KISS, DRY, etc.
5. GoF шаблони. Розкажи 3 детально.
6. decoupling/слаба зв'язність. Як це виглядає на практиці.
7. В чому проблема з "new"? Factory, Dependency Injection. 
8. Які проблеми вирішує DI?
9. Є клас А з єдиним конструктором A(B b). І є клас В з єдиним конструктором В(А а). Як Спрінг інжектить А в В і В в А.

### Servlets

1. Життєвий цикл сервлета.
2. Крім сервлета, що ще існує в Servlets API.
3. Що таке Http сесія?
4. Що таке JSESSIONID?
5. ...

### JDBC

1. Як створити транзакцію вручну? А потім виконати rollback?
2. Statement vs PrepareStatement.
3. ...

### SQL

1. ...
2. Рівні ізоляції транзакцій.

### Hibernate

1. Які функції виконує сесія хібернейта?
2. Entity states
3. Second vs first level cache.
4. Як технічно хібернейт конвертує запис в об'єкт?
5. Lazy loading
6. Мапінг ієрархії класів на таблиці.
7. Анотація Version
8. Оптимістичне блокування
9. ... 


