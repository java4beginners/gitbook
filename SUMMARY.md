# Summary

* [Вступ](README.md)

* [Шлях Java Junior](chapters/java-path.md)
* [Співбесіда](chapters/interview.md)
---
* Java Core
  
  * Типи Даних
    * [char](chapters/core/char.md)
    * [binary](chapters/core/binary.md)
    * [String](chapters/core/string.md)

  * Основні Структури Даних
    * [Linked Lists](chapters/algo/linked-list.md)
    * [Dynamic Array](chapters/algo/dynamic-array.md)
    * [Hash Map](chapters/algo/hashmap.md)
    * [Heap](chapters/algo/heap.md)
    * [Stack](chapters/algo/stack.md)
    * [Queue](chapters/algo/queue.md)
  
  * Синтаксис
    * [Generics](chapters/core/generics.md)
  
  * JDK API
    * [I/O Streams + OOP](chapters/core/oop.md)
    * Theads
---
* [Git](chapters/git.md)
* [Maven](chapters/maven.md)
---
* JEE
  * [Сервлети](chapters/servlets.md)
  * [SQL](chapters/sql.md)
  * [JDBC](chapters/jdbc.md)
  * [Spring](chapters/spring.md)
  * [JPA / Hibernate](chapters/jpa.md)

