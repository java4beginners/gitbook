# Generics 

Уявімо, що є клас який використовує в своєму коді інші класи. Так от в Java можна це параметризувати і для одного і того ж класу підставляти різні залежні типи.

Наприклад є оголошений клас `ArrayList<T>` і замість `T` ми можемо підставляти різні типи, `new ArrayList<String>(), new ArrayList<Integer>()`.

[Читати офіційну документацію.](https://docs.oracle.com/javase/tutorial/java/generics/types.html)  
[Читати на Wikipedia](https://uk.wikipedia.org/wiki/%D0%A3%D0%B7%D0%B0%D0%B3%D0%B0%D0%BB%D1%8C%D0%BD%D0%B5%D0%BD%D0%BD%D1%8F_%D0%B2_Java)  
[На сайті Baeldung.com](https://www.baeldung.com/java-generics)

### Задача

Переписати LinkedList, DtnamicArray, Heap, Stack, Queue на узагальнення/generics, щоб ми могли зберігати не тільки цілі числа, а будьякі об'єкти.
